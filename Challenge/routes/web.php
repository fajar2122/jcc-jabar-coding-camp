<?php

use Illuminate\Support\Facades\Route;

// controller
use App\Http\Controllers\DashboardCT;
use App\Http\Controllers\DashboardAdminCT;
use App\Http\Controllers\LoginCT;
use App\Http\Controllers\ProfileCT;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//user
Route::get('/', [DashboardCT::class, 'index'])->name('dashboard.index');
Route::get('/profile', [ProfileCT::class, 'profile'])->name('profile.index');
Route::get('/profile-struktural', [ProfileCT::class, 'struktural'])->name('profile.struktural');


//admin
Route::get('/login', [LoginCT::class, 'login'])->name('login');
Route::post('/authenticate', [LoginCT::class, 'authenticate'])->name('authenticate');
Route::get('/logout', [LoginCT::class, 'signOut'])->name('logout');

Route::group(['middleware' => ['auth', 'checkRole:admin']],function(){
 Route::get('/sekolah-app', [DashboardAdminCT::class, 'index'])->name('sekolah.index');
});
