<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileCT extends Controller
{
    public function profile()
    {
        return view('user.profile');
    }

    public function struktural()
    {
        return view('user.profile-struktural');
    }

}
