<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Basic Page Needs
================================================== -->
  <meta charset="utf-8">
  <title>SMK Negeri 1 Rawamerta - Kreatif, Inovatif dan Profesional</title>

  <!-- Mobile Specific Metas
================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Construction Html5 Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">

  <!-- Favicon
================================================== -->
  <link rel="icon" type="image/logosmk.jpeg" href="{{ URL::asset('public/assets/images/logosmk.jpeg')}}">

  <!-- CSS
================================================== -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/bootstrap/bootstrap.min.css')}}">
  <!-- FontAwesome -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/fontawesome/css/all.min.css')}}">
  <!-- Animation -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/animate-css/animate.css')}}">
  <!-- slick Carousel -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/slick/slick.css')}}">
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/slick/slick-theme.css')}}">
  <!-- Colorbox -->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/plugins/colorbox/colorbox.css')}}">
  <!-- Template styles-->
  <link rel="stylesheet" href="{{ URL::asset('public/assets/css/style.css')}}">

</head>

<body>
  <div class="body-inner">

    <div id="top-bar" class="top-bar">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8">
            <ul class="top-info text-center text-md-left">
              <li><i class="fas fa-map-marker-alt"></i>
                <p class="info-text">SMK Negeri 1 Rawamerta</p>
              </li>
            </ul>
          </div>
          <!--/ Top info end -->

          <div class="col-lg-4 col-md-4 top-social text-center text-md-right">
            <ul class="list-unstyled">
              <li>
                <a title="Facebook" href="https://facebbok.com/themefisher.com">
                  <span class="social-icon"><i class="fab fa-facebook-f"></i></span>
                </a>
                <a title="Twitter" href="https://twitter.com/themefisher.com">
                  <span class="social-icon"><i class="fab fa-twitter"></i></span>
                </a>
                <a title="Instagram" href="https://instagram.com/themefisher.com">
                  <span class="social-icon"><i class="fab fa-instagram"></i></span>
                </a>
                <a title="Linkdin" href="https://github.com/themefisher.com">
                  <span class="social-icon"><i class="fab fa-github"></i></span>
                </a>
              </li>
            </ul>
          </div>
          <!--/ Top social end -->
        </div>
        <!--/ Content row end -->
      </div>
      <!--/ Container end -->
    </div>
    <!--/ Topbar end -->
    <!-- Header start -->
    <header id="header" class="header-one">
      <div class="bg-white">
        <div class="container">
          <div class="logo-area">
            <div class="row align-items-center">
              <div class="logo col-lg-3 text-center text-lg-left mb-3 mb-md-5 mb-lg-0">
                <a class="d-block" href="{{ route('dashboard.index') }}">
                  <img style="width: 300px; height: 50px;" loading="lazy" src="{{ URL::asset('public/assets/images/footer-logosmk.png')}}" alt="SMKN 1 Rawamerta">
                </a>
              </div><!-- logo end -->

              <div class="col-lg-9 header-right">
                <ul class="top-info-box">
                  <li>
                    <div class="info-box">
                      <div class="info-box-content">
                        <p class="info-box-title">Telepon</p>
                        <p class="info-box-subtitle"><a href="tel:(+9) 847-291-4353">0000 (00000)</a></p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="info-box">
                      <div class="info-box-content">
                        <p class="info-box-title">E-mail</p>
                        <p class="info-box-subtitle"><a href="mailto:office@Constra.com">smkn1rawamerta@gmail.com</a></p>
                      </div>
                    </div>
                  </li>

                  <li class="last">
                    <div class="info-box last">
                      <div class="info-box-content">
                        <p class="info-box-title">NPSN</p>
                        <p class="info-box-subtitle">(60728562)</p>
                      </div>
                    </div>
                  </li>
                  <li class="header-get-a-quote">
                    <a class="btn btn-primary" href="contact.html">Kontak</a>
                  </li>
                </ul><!-- Ul end -->
              </div><!-- header right end -->
            </div><!-- logo area end -->

          </div><!-- Row end -->
        </div><!-- Container end -->
      </div>

      <div class="site-navigation">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <nav class="navbar navbar-expand-lg navbar-dark p-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div id="navbar-collapse" class="collapse navbar-collapse">
                  <ul class="nav navbar-nav mr-auto">
                    <li class="nav-item dropdown active">
                      <a href="{{ route('dashboard.index') }}" class="nav-link dropdown-toggle" data-toggle="">Beranda</a>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="">Profil
                        <!-- <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ route('profile.index') }}">Tentang Sekolah</a></li>
                        <li><a href="{{route('profile.struktural') }}">Struktur Organisasi</a></li>
                      </ul>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="">Jurusan
                        <!-- <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="tabog.html">Otomasisasi & Tata Kelola Perkantoran</a></li>
                        <li><a href="tabus.html">Teknik Bisnis & Sepeda Motor</a></li>
                        <li><a href="tacin.html">Teknik Kendaraan Ringan Otomotif</a></li>
                        <li><a href="tkj.html">Teknik Komputer & Jaringan</a></li>
                        <li><a href="hotel.html">Teknik Instalasi Tenaga Listrik</a></li>
                      </ul>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="">SDM
                        <!--  <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="dirgur.html">Deriktori Guru</a></li>
                        <li><a href="dirtu.html">Direktori Tata Usaha</a></li>
                      </ul>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="">Kesiswaan
                        <!-- <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="presis.html">Prestasi Siswa</a></li>
                        <li><a href="statis.html">Statistik Kesiswaan</a></li>
                        <li class="dropdown-submenu">
                          <a href="#!" class="dropdown-toggle" data-toggle="">Ekstrakulikuler</a>
                          <ul class="dropdown-menu">
                            <li><a href="bikes.html">English Club</a></li>
                            <li><a href="biro.html">Futsal</a></li>
                            <li><a href="bitek.html">Jaipong</a></li>
                          </ul>
                        </li>
                      </ul>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="">PORTAL NESART
                        <!-- <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="pengumuman.html">Pengumuman</a></li>
                        <li><a href="kabar.html">Kabar</a></li>
                        <li><a href="agenda.html">Agenda</a></li>

                      </ul>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">BKK
                        <!--  <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="loker.html">Loker</a></li>
                        <li><a href="tloker.html">Data Kerjasama Perusahaan</a></li>
                      </ul>
                    </li>

                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">ALUMNI
                        <!--  <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="pena.html">Penelusuran Alumni</a></li>
                      </ul>
                    </li>
                    <li class="nav-item dropdown">
                      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">PROGRAM KERJA
                        <!--  <i class="fa fa-angle-down"></i> -->
                      </a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="pena.html">Program Kurikulum</a></li>
                        <li><a href="pena.html">Program Kesiswaan</a></li>
                        <li><a href="pena.html">Program Sarpras</a></li>
                        <li><a href="pena.html">Program Hubin</a></li>
                        <li><a href="pena.html">Program Jurusan</a></li>
                      </ul>
                    </li>

                    <li class="nav-item"><a class="nav-link" href="contact.html">Kontak</a></li>
                  </ul>
                </div>
              </nav>
            </div>
            <!--/ Col end -->
          </div>
          <!--/ Row end -->

          <div class="search-block" style="display: none;">
            <label for="search-field" class="w-100 mb-0">
              <input type="text" class="form-control" id="search-field" placeholder="Type what you want and enter">
            </label>
            <span class="search-close">&times;</span>
          </div><!-- Site search end -->
        </div>
        <!--/ Container end -->

      </div>
      <!--/ Navigation end -->
    </header>
    <!--/ Header end -->

    @yield('content-user')


    <footer id="footer" class="footer bg-overlay">
      <div class="footer-main">
        <div class="container">
          <div class="row justify-content-between">
            <div class="col-lg-4 col-md-6 footer-widget footer-about">
              <h3 class="widget-title">SMK Negeri 1 Rawamerta</h3>
              <img loading="lazy" width="200px" class="footer-logo" src="{{ URL::asset('public/assets/images/footer-logosmk1.png')}}" alt="Constra">
              <p>Jl. Raya Pasir Kaliki, Pasirkaliki, Rawamerta. Kabupaten Karawang, Jawab Barat 41382</p>
              <div class="footer-social">
                <ul>
                  <li><a href="#" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                  </li>
                  <li><a href="#" aria-label="Instagram"><i class="fab fa-instagram"></i></a></li>
                  <li><a href="#" aria-label="Github"><i class="fab fa-youtube"></i></a></li>
                </ul>
              </div><!-- Footer social end -->
            </div><!-- Col end -->


            <div class="col-lg-3 col-md-6 mt-5 mt-lg-0 footer-widget">
              <h3 class="widget-title">Jaringan</h3>
              <ul class="list-arrow">
                <li><a href="service-single.html">eRapor SMK</a></li>
                <li><a href="service-single.html">eLibrary</a></li>
                <li><a href="service-single.html">PPDB 2021</a></li>
                <li><a href="service-single.html">Blog Eksul</a></li>
                <li><a href="service-single.html">LSP SMK Negeri Rawamerta</a></li>
              </ul>
            </div><!-- Col end -->

            <div class="col-lg-3 col-md-6 mt-5 mt-lg-0 footer-widget">
              <h3 class="widget-title">Terbaru</h3>
              <ul class="list-arrow">
                <li><a href="service-single.html">Pengumuman Kelulusan Tahun 2022</a></li>
                <li><a href="service-single.html">Open Donasi Peduli Bencana Alam</a></li>
                <li><a href="service-single.html"></a></li>
                <li><a href="service-single.html"></a></li>
              </ul>
            </div><!-- Col end -->
          </div><!-- Row end -->
        </div><!-- Container end -->
      </div><!-- Footer main end -->

      <div class="copyright">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-md-6">
              <div class="copyright-info text-center text-md-left">
                <span>Copyright &copy; <script>
                    document.write(new Date().getFullYear())
                  </script>, SMK Negeri 1 <a href="https://themefisher.com">Rawamerta</a></span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="footer-menu text-center text-md-right">
                <ul class="list-unstyled">
                  <li><a href="about.html">Masuk</a></li>
                  <li><a href="team.html">Tim Pengelola</a></li>
                  <li><a href="contact.html">Kontak</a></li>
                  <li><a href="news-left-sidebar.html">Dikembangkan oleh...</a></li>
                </ul>
              </div>
            </div>
          </div><!-- Row end -->

          <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top position-fixed">
            <button class="btn btn-primary" title="Back to Top">
              <i class="fa fa-angle-double-up"></i>
            </button>
          </div>

        </div><!-- Container end -->
      </div><!-- Copyright end -->
    </footer><!-- Footer end -->


    <!-- Javascript Files
  ================================================== -->

    <!-- initialize jQuery Library -->
    <script src="{{ URL::asset('public/assets/plugins/jQuery/jquery.min.js')}}"></script>
    <!-- Bootstrap jQuery -->
    <script src="{{ URL::asset('public/assets/plugins/bootstrap/bootstrap.min.js')}}" defer></script>
    <!-- Slick Carousel -->
    <script src="{{ URL::asset('public/assets/plugins/slick/slick.min.js')}}"></script>
    <script src="{{ URL::asset('public/assets/plugins/slick/slick-animation.min.js')}}"></script>
    <!-- Color box -->
    <script src="{{ URL::asset('public/assets/plugins/colorbox/jquery.colorbox.js')}}"></script>
    <!-- shuffle -->
    <script src="{{ URL::asset('public/assets/plugins/shuffle/shuffle.min.js')}}" defer></script>


    <!-- Google Map API Key-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU" defer></script>
    <!-- Google Map Plugin-->
    <script src="{{ URL::asset('public/assets/plugins/google-map/map.js')}}" defer></script>

    <!-- Template custom -->
    <script src="{{ URL::asset('public/assets/js/script.js')}}"></script>

  </div><!-- Body inner end -->
</body>

</html>