@extends('base.master-user')
@section('content-user')

<div class="banner-carousel banner-carousel-1 mb-0">
  <div class="banner-carousel-item" style="background-image:url(public/assets/images/banner/foto7.jpg)">
    <div class="slider-content">
      <div class="container h-100">
        <div class="row align-items-center h-100">
          <div class="col-md-12 text-center">
            <h3 class="slide-sub-title" data-animation-in="slideInRight">SMK Negeri 1 Rawamerta</h3>
            <p data-animation-in="slideInLeft" data-duration-in="1.2">
              <a href="contact.html" class="slider btn btn-primary border">Contact</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="banner-carousel-item" style="background-image:url(public/assets/images/banner/foto10.jpg)">
    <div class="slider-content text-left">
      <div class="container h-100">
        <div class="row align-items-center h-100">
          <div class="col-md-12">
            <h2 class="slide-title-box" data-animation-in="slideInDown">SMK Negeri 1 Rawamerta</h2>
            <h3 class="slide-title" data-animation-in="fadeIn">Mengantar Siswa PKL ke UBP Karawang</h3>
            <h3 class="slide-sub-title" data-animation-in="slideInLeft">SMKN 1 RAWAMERTA</h3>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="banner-carousel-item" style="background-image:url(public/assets/images/banner/foto3.jpg)">
    <div class="slider-content text-right">
      <div class="container h-100">
        <div class="row align-items-center h-100">
          <div class="col-md-12">
            <h2 class="slide-title" data-animation-in="slideInDown">SMK Negeri 1 Rawamerta</h2>
            <h3 class="slide-sub-title" data-animation-in="fadeIn">Kegiatan Rohani Setiap Hari Jumat</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section id="main-container" class="main-container">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mt-0">
        <h3 class="column-title">SMK Negeri 1 Rawamerta</h3>
        <p>Pandangan atau gagasan yang disampaikan oleh beberapa tokoh masyarakat, bahwa di Kecamatan Rawamerta sudah saatnya memiliki SLTA Negeri. Mengingat kecamatan lain, seperti : Kecamatan Tempuran, Pangkalan, Pakisjaya, Tirtajaya, Jayakerta, Kutawaluya, dan Wadas. Masing-masing telah memiliki SLTA Negeri.</p>
        <blockquote>
          <p>Surat Keputusan Bupati Karawang nomor : 421.3/Kep. 594 – Huk / 2012. Tertanggal 1 Juni 2012 tentang Unit Sekolah Baru (USB) Sekolah Menengah Kejuruan Negeri (SMKN 1 Rawamerta). Dan Tahun Pelajaran 2012/2013 dimulai Penerimaan Peserta Didik Baru. Dengan program keahlian Teknik Komputer dan Jaringan (TKJ) dan Teknik Sepeda Motor (TSM). </p>
        </blockquote>
      </div><!-- Col end -->

      <div class="col-lg-6 mt-5 mt-lg-5">

        <div id="page-slider" class="page-slider small-bg">

          <div class="item" style="background-image:url('public/assets/images/slider-pages/foto6.jpg')">
            <div class="container">
              <div class="box-slider-content">
                <div class="box-slider-text">
                  <h2 class="box-slide-title">Pembelajaran</h2>
                </div>
              </div>
            </div>
          </div><!-- Item 1 end -->

          <div class="item" style="background-image:url('public/assets/images/slider-pages/foto9.jpg')">
            <div class="container">
              <div class="box-slider-content ">
                <div class="box-slider-text">
                  <h2 class="box-slide-title">Sosialisasi</h2>
                </div>
              </div>
            </div>
          </div><!-- Item 1 end -->

          <div class="item" style="background-image:url('public/assets/images/slider-pages/foto7.jpg')">
            <div class="container">
              <div class="box-slider-content">
                <div class="box-slider-text">
                  <h2 class="box-slide-title">Gedung Sekolah</h2>
                </div>
              </div>
            </div>
          </div><!-- Item 1 end -->
        </div><!-- Page slider end-->


      </div><!-- Col end -->
    </div><!-- Content row end -->

  </div><!-- Container end -->
</section><!-- Main container end -->


<!--/ Title row end -->

</section><!-- Service end -->

<section id="facts" class="facts-area dark-bg">
  <div class="container">
    <div class="facts-wrapper">
      <div class="row">
        <div class="col-md-3 col-sm-6 ts-facts">
          <div class="ts-facts-img">
            <img style="width:70px;" loading="lazy" src="{{ asset('public/assets/images/icon-image/students.png')}}" alt="facts-img">
          </div>
          <div class="ts-facts-content">
            <h2 class="ts-facts-num"><span class="counterUp" data-count="1789">0</span></h2>
            <h3 class="ts-facts-title">Siswa dan Siswi</h3>
          </div>
        </div><!-- Col end -->

        <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-sm-0">
          <div class="ts-facts-img">
            <img style="width:70px" loading="lazy" src="{{ asset('public/assets/images/icon-image/presentation.png')}}" alt="facts-img">
          </div>
          <div class="ts-facts-content">
            <h2 class="ts-facts-num"><span class="counterUp" data-count="647">0</span></h2>
            <h3 class="ts-facts-title">Guru Sekolah</h3>
          </div>
        </div><!-- Col end -->

        <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
          <div class="ts-facts-img">
            <img style="width:70px" loading="lazy" src="{{ asset('public/assets/images/icon-image/support.png')}}" alt="facts-img">
          </div>
          <div class="ts-facts-content">
            <h2 class="ts-facts-num"><span class="counterUp" data-count="4000">0</span></h2>
            <h3 class="ts-facts-title">Staff Admin</h3>
          </div>
        </div><!-- Col end -->

        <div class="col-md-3 col-sm-6 ts-facts mt-5 mt-md-0">
          <div class="ts-facts-img">
            <img style="width:70px" loading="lazy" src="{{ asset('public/assets/images/icon-image/engineering.png')}}" alt="facts-img">
          </div>
          <div class="ts-facts-content">
            <h2 class="ts-facts-num"><span class="counterUp" data-count="44">0</span></h2>
            <h3 class="ts-facts-title">Jurusan</h3>
          </div>
        </div><!-- Col end -->

      </div> <!-- Facts end -->
    </div>
    <!--/ Content row end -->
  </div>
  <!--/ Container end -->
</section><!-- Facts end -->

<section id="news" class="news">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <h1 class="section-title">Berita Terbaru</h1>
        <h3 class="section-sub-title">SMKN 1 Rawamerta</h3>
      </div>
    </div>
    <!--/ Title row end -->

    <div class="row">
      <div class="col-lg-4 col-md-6 mb-4">
        <div class="latest-post">
          <div class="latest-post-media">
            <a href="berita.html" class="latest-post-img">
              <img loading="lazy" class="img-fluid" src="{{ asset('public/assets/images/news/img1.jpg')}}" alt="img">
            </a>
          </div>
          <div class="post-body">
            <h4 class="post-title">
              <a href="berita.html" class="d-inline-block">Seorang Siswa SMKN 1 Rawamerta Mendapatkan Juara 1 Nasional.</a>
            </h4>
            <div class="latest-post-meta">
              <span class="post-item-date">
                <i class="fa fa-clock-o"></i> July 20, 2021
              </span>
            </div>
          </div>
        </div><!-- Latest post end -->
      </div><!-- 1st post col end -->

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="latest-post">
          <div class="latest-post-media">
            <a href="berita.html" class="latest-post-img">
              <img loading="lazy" class="img-fluid" src="{{ asset('public/assets/images/news/img1.jpg')}}" alt="img">
            </a>
          </div>
          <div class="post-body">
            <h4 class="post-title">
              <a href="berita.html" class="d-inline-block">Seorang Siswa SMKN 1 Rawamerta Mendapatkan Juara 1 Nasional.</a>
            </h4>
            <div class="latest-post-meta">
              <span class="post-item-date">
                <i class="fa fa-clock-o"></i> June 17, 2021
              </span>
            </div>
          </div>
        </div><!-- Latest post end -->
      </div><!-- 2nd post col end -->

      <div class="col-lg-4 col-md-6 mb-4">
        <div class="latest-post">
          <div class="latest-post-media">
            <a href="berita.html" class="latest-post-img">
              <img loading="lazy" class="img-fluid" src="{{ asset('public/assets/images/news/img1.jpg')}}" alt="img">
            </a>
          </div>
          <div class="post-body">
            <h4 class="post-title">
              <a href="berita.html" class="d-inline-block">Seorang Siswa SMKN 1 Rawamerta Mendapatkan Juara 1 Nasional.</a>
            </h4>
            <div class="latest-post-meta">
              <span class="post-item-date">
                <i class="fa fa-clock-o"></i> Aug 13, 2021
              </span>
            </div>
          </div>
        </div><!-- Latest post end -->
      </div><!-- 3rd post col end -->
    </div>
    <!--/ Content row end -->

    <div class="general-btn text-center mt-4">
      <a class="btn btn-primary" href="news-left-sidebar.html">Selengkapnya</a>
    </div>

  </div>
  <!--/ Container end -->
</section>
<!--/ News end -->
<section id="project-area" class="project-area solid-bg">
  <div class="container">
    <div class="row text-center">
      <div class="col-lg-12">
        <h2 class="section-title">Galeri</h2>
        <h3 class="section-sub-title">SMKN 1 Rawamerta</h3>
      </div>
    </div>
    <!--/ Title row end -->

    <div class="row text-center">
      <div class="col-12">


        <div class="row shuffle-wrapper">
          <div class="col-1 shuffle-sizer"></div>

          <div class="col-lg-4 col-sm-6 shuffle-item" data-groups="[&quot;government&quot;,&quot;healthcare&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" href="{{ asset('public/assets/images/projects/foto1.jpg')}}" aria-label="project-img">
                <img style="width:700px; height:254px;" class="img-fluid" src="{{ asset('public/assets/images/projects/foto1.jpg')}}" alt="project-img">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>

            </div>
          </div><!-- shuffle item 1 end -->

          <div class="col-lg-4 col-sm-6 shuffle-item" data-groups="[&quot;healthcare&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" href="{{ asset('public/assets/images/projects/img9.jpeg')}}" aria-label="project-img">
                <img style="width:700px; height:254px;" class="img-fluid" src="{{ asset('public/assets/images/projects/img9.jpeg')}}" alt="project-img">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>
            </div>
          </div><!-- shuffle item 2 end -->

          <div class="col-lg-4 col-sm-6 shuffle-item" data-groups="[&quot;infrastructure&quot;,&quot;commercial&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" href="{{ asset('public/assets/images/projects/foto3.jpg')}}" aria-label="project-img">
                <img style="width:700px; height:254px;" class="img-fluid" src="{{ asset('public/assets/images/projects/foto3.jpg')}}" alt="project-img">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>

            </div>
          </div><!-- shuffle item 3 end -->

          <div class="col-lg-4 col-sm-6 shuffle-item" data-groups="[&quot;education&quot;,&quot;infrastructure&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" href="{{ asset('public/assets/images/projects/foto4.jpg')}}" aria-label="project-img">
                <img style="width:700px; height:254px;" class="img-fluid" src="{{ asset('public/assets/images/projects/foto4.jpg')}}" alt="project-img">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>

            </div>
          </div><!-- shuffle item 4 end -->

          <div class="col-lg-4 col-sm-6 shuffle-item" data-groups="[&quot;infrastructure&quot;,&quot;education&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" href="{{ asset('public/assets/images/projects/foto5.jpg')}}" aria-label="project-img">
                <img style="width:700px; height:254px;" class="img-fluid" src="{{ asset('public/assets/images/projects/foto5.jpg')}}" alt="project-img">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>

            </div>
          </div><!-- shuffle item 5 end -->

          <div class="col-lg-4 col-sm-6 shuffle-item" data-groups="[&quot;residential&quot;]">
            <div class="project-img-container">
              <a class="gallery-popup" href="{{ asset('public/assets/images/projects/foto99.jpg')}}" aria-label="project-img">
                <img style="width:700px; height:254px;" class="img-fluid" src="{{ asset('public/assets/images/projects/foto99.jpg')}}" alt="project-img">
                <span class="gallery-icon"><i class="fa fa-plus"></i></span>
              </a>

            </div>
          </div><!-- shuffle item 6 end -->
        </div><!-- shuffle end -->
      </div>

      <div class="col-12">
        <div class="general-btn text-center">
          <a class="btn btn-primary" href="galerisemua.html">Lihat Semua</a>
        </div>
      </div>

    </div><!-- Content row end -->
  </div>
  <!--/ Container end -->
</section><!-- Project area end -->

<section class="content alig">
  <div class="container">
    <div class="row">

      <div class="col-lg-18 mt-5 mt-lg-0">

        <h3 class="column-title text-center">Hubungan Industri</h3>

        <div class="row all-clients">
          <div class="col-sm-4 col-6">
            <figure class="clients-logo">
              <a href="#!"><img loading="lazy" class="img-fluid" src="images/clients/client1.png" alt="clients-logo" /></a>
            </figure>
          </div><!-- Client 1 end -->

          <div class="col-sm-4 col-6">
            <figure class="clients-logo">
              <a href="#!"><img loading="lazy" class="img-fluid" src="images/clients/client2.png" alt="clients-logo" /></a>
            </figure>
          </div><!-- Client 2 end -->

          <div class="col-sm-4 col-6">
            <figure class="clients-logo">
              <a href="#!"><img loading="lazy" class="img-fluid" src="images/clients/client3.png" alt="clients-logo" /></a>
            </figure>
          </div><!-- Client 3 end -->

          <div class="col-sm-4 col-6">
            <figure class="clients-logo">
              <a href="#!"><img loading="lazy" class="img-fluid" src="images/clients/client4.png" alt="clients-logo" /></a>
            </figure>
          </div><!-- Client 4 end -->

          <div class="col-sm-4 col-6">
            <figure class="clients-logo">
              <a href="#!"><img loading="lazy" class="img-fluid" src="images/clients/client5.png" alt="clients-logo" /></a>
            </figure>
          </div><!-- Client 5 end -->

          <div class="col-sm-4 col-6">
            <figure class="clients-logo">
              <a href="#!"><img loading="lazy" class="img-fluid" src="images/clients/client6.png" alt="clients-logo" /></a>
            </figure>
          </div><!-- Client 6 end -->

        </div><!-- Clients row end -->

      </div><!-- Col end -->

    </div>
    <!--/ Content row end -->
  </div>
  <!--/ Container end -->
</section><!-- Content end -->
@endsection