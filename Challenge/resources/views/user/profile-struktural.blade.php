@extends('base.master-user')
@section('content-user')
<section id="main-container" class="main-container pb-4">
 <div class="container">
  <div class="row text-center">
   <div class="col-lg-12">
    <h3 class="section-sub-title mb-1">Struktur Organisasi</h3>
    <h3 class="section-sub-title ">SMKN 1 Rawamerta</h3>

   </div>
  </div>
  <!--/ Title row end -->

  <div class="row justify-content-center text-center">
   <div class="col-lg-3 col-sm-6 mb-5">
    <div class="ts-team-wrapper">
     <div class="team-img-wrapper">
      <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/1s.jpg')}}" class="img-fluid" alt="team-img">
     </div>
     <div class="ts-team-content-classic">
      <h3 class="ts-name">kepala sekolah</h3>
      <p class="ts-designation">H. Rosli, S.Pd</p>
      <div class="team-social-icons">
       <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
       <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
       <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
      </div>
      <!--/ social-icons-->
     </div>
    </div>
    <!--/ Team wrapper 1 end -->

   </div><!-- Col end -->

   

  </div><!-- Content row 1 end -->
  <div class="row justify-content-center text-center">
   <div class="col-lg-3 col-sm-6 mb-5">
    <div class="ts-team-wrapper">
     <div class="team-img-wrapper">
      <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/kasubag.jpeg')}}" class="img-fluid" alt="team-img">
     </div>
     <div class="ts-team-content-classic">
      <h3 class="ts-name">kasubag. tu</h3>
      <p class="ts-designation">Cecep Pathurohman</p>
      <div class="team-social-icons">
       <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
       <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
       <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
      </div>
      <!--/ social-icons-->
     </div>
    </div>
    <!--/ Team wrapper 1 end -->

   </div><!-- Col end -->
   <div class="row justify-content-center text-center">
   <div class="col-lg-3 col-sm-6 mb-5">
    <div class="ts-team-wrapper">
     <div class="team-img-wrapper">
      <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/1b.jpg')}}" class="img-fluid" alt="team-img">
     </div>
     <div class="ts-team-content-classic">
      <h3 class="ts-name">bendahara sekolah</h3>
      <p class="ts-designation">Eneng Rohaeti, SE</p>
      <div class="team-social-icons">
       <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
       <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
       <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
      </div>
      <!--/ social-icons-->
     </div>
    </div>
    <!--/ Team wrapper 2 end -->
   </div><!-- Col end -->

   <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/1wkuri.jpg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">wakasek kurikulum</h3>
       <p class="ts-designation">Sakhina Sri Utami, S.Pd</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 3 end -->
    </div><!-- Col end -->

    <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/team4.jpg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">wakasek bid.hubinmas</h3>
       <p class="ts-designation">Eka Subekti, M.Pd</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 4 end -->

    </div><!-- Col end -->

    <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/1wsiswa.jpg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">wakasek bid.kesiswaan</h3>
       <p class="ts-designation">Siti Nurhayati, M.Pd</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 5 end -->
    </div><!-- Col end -->

    <div class="col-lg-3 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/sarpas.jpeg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">wakasek bid.sar-pras</h3>
       <p class="ts-designation">A. Solihudim, M.Pd. I</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 6 end -->
    </div><!-- Col end -->
   </div><!-- Content row end -->

<div class="row">
    <div class="col-lg-2 col-md-4 col-sm-6 mb-2">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/tkj.jpeg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">Ketua Jurusan TKJ</h3>
       <p class="ts-designation">Joko Yudhanto, M.Kom. Gr</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-google-plus"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 3 end -->
    </div><!-- Col end -->

    <div class="col-lg-2 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/tkro.jpeg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">Ketua Jurusan TKRO</h3>
       <p class="ts-designation">Suhendar, S.Pd</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 4 end -->

    </div><!-- Col end -->

<div class="col-lg-2 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/titl.jpeg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">Ketua Jurusan TITL</h3>
       <p class="ts-designation">A. Irwandi, S.T</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 4 end -->

    </div><!-- Col end -->

    <div class="col-lg-2 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/otkp.jpeg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">Ketua Jurusan OTKP</h3>
       <p class="ts-designation">Nimar, S.Pd</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 4 end -->

    </div><!-- Col end -->

    <div class="col-lg-2 col-md-4 col-sm-6 mb-5">
     <div class="ts-team-wrapper">
      <div class="team-img-wrapper">
       <img style="width:150px; height:200px;" loading="lazy" src="{{ asset('public/assets/images/team/tbsm.jpg')}}" class="img-fluid" alt="team-img">
      </div>
      <div class="ts-team-content-classic">
       <h3 class="ts-name">Ketua Jurusan TBSM</h3>
       <p class="ts-designation">Hepi Supriyadi, S.Pd. M.T.</p>
       <div class="team-social-icons">
        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
       </div>
       <!--/ social-icons-->
      </div>
     </div>
     <!--/ Team wrapper 4 end -->

    </div><!-- Col end -->

  </div><!-- Container end -->
</section><!-- Main container end -->
@endsection