@extends('base.master-user')
@section('content-user')

<section id="main-container" class="main-container">
 <div class="container">

  <div class="row">
   <div class="col-lg-12">
    <h3 class="border-title border-center text-center">Profil Sekolah</h3>

    <div class="accordion accordion-group accordion-classic" id="construction-accordion">
     <div class="card">
      <div class="card-header p-0 bg-transparent" id="headingOne">
       <h2 class="mb-0">
        <button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         Profil SMKN 1 Rawamerta
        </button>
       </h2>
      </div>

      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#construction-accordion">
       <div class="card-body">
        <table class="table table-hover">
         <tr>
          <td colspan="12" style="background-color:black; color:white;">1. Identitas</td>
         </tr>
         <tr>
          <td class="col-0">1</td>
          <td class="col-4">Nama Sekolah</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">SMKN 1 RAWAMERTA</td>
         </tr>
         <tr>
          <td class="col-0">2</td>
          <td class="col-4">NPSN</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">60728562</td>
         </tr>
         <tr>
          <td class="col-0">3</td>
          <td class="col-4">Jenjang Pendidikan</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">SMK</td>
         </tr>
         <tr>
          <td class="col-0">4</td>
          <td class="col-4">Status Sekolah</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Negeri</td>
         </tr>
         <tr>
          <td class="col-0">5</td>
          <td class="col-4">Alamat Sekolah</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Jl. Raya Rawamerta Desa Pasirkaliki Kecamatan Rawamerta - Kabupaten Karawang</td>
         </tr>
         <tr>
          <td></td>
          <td>RT/RW</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">5 / 3</td>
         </tr>
         <tr>
          <td></td>
          <td>Kode Pos</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">41382</td>
         </tr>
         <tr>
          <td></td>
          <td>Kelurahan</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Pasirkaliki</td>
         </tr>
         <tr>
          <td></td>
          <td>Kecamatan</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Kec. Rawamerta</td>
         </tr>
         <tr>
          <td></td>
          <td>Kabupaten/Kota</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Kab. Karawang</td>
         </tr>
         <tr>
          <td></td>
          <td>Provinsi</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Prov. Jawa Barat</td>
         </tr>
         <tr>
          <td></td>
          <td>Negara</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Indonesia</td>
         </tr>
         <tr>
          <td>6</td>
          <td>Posisi Geografis</td>
          <td class="col-0">:</td>
          <td class="col-4">-6.2541</td>
          <td class="col-4">Lintang</td>
         </tr>
         <tr>
          <td></td>
          <td></td>
          <td class="col-0"></td>
          <td class="col-4">107.3418</td>
          <td class="col-4">Bujur</td>
         </tr>

        </table>
        <table class="table table-bordered">
         <tr>
          <td colspan="12" style="background-color:black; color:white;">2. Data Lengkap</td>
         </tr>
         <tr>
          <td class="col-0">7</td>
          <td class="col-4">SK Pendirian Sekolah</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">421.3/Kep.594-Huk/2012</td>
         </tr>
         <tr>
          <td class="col-0">8</td>
          <td class="col-4">Tanggal SK Pendirian</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">-</td>
         </tr>
         <tr>
          <td class="col-0">9</td>
          <td class="col-4">Status Kepemilikan</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Pemerintah Daerah</td>
         </tr>
         <tr>
          <td class="col-0">10</td>
          <td class="col-4">SK Izin Operasional</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">421.1/KEP594-HUK/2012</td>
         </tr>
         <tr>
          <td class="col-0">11</td>
          <td class="col-4">Nama Sekolah</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">SMKN 1 RAWAMERTA</td>
         </tr>
         <tr>
          <td class="col-0">12</td>
          <td class="col-4">Tgl SK Izin Operasional</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">-</td>
         </tr>
         <tr>
          <td class="col-0">13</td>
          <td class="col-4">Nomor Rekening</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">0076577341001</td>
         </tr>
         <tr>
          <td class="col-0">14</td>
          <td class="col-4">Nama Bank</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">BPD JABAR BANTEN...</td>
         </tr>
         <tr>
          <td class="col-0">15</td>
          <td class="col-4">Cabang KCP/Unit</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">BPD JABAR BANTEN CABANG KARAWANG...</td>
         </tr>
         <tr>
          <td class="col-0">16</td>
          <td class="col-4">Rekening Atas Nama</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">BOSSMKN1RAWAMERTA...</td>
         </tr>
         <tr>
          <td class="col-0">17</td>
          <td class="col-4">MBS</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">YA</td>
         </tr>
         <tr>
          <td class="col-0">18</td>
          <td class="col-4">Memungut Iuran</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Tidak</td>
         </tr>
         <tr>
          <td class="col-0">19</td>
          <td class="col-4">Nominal/Siswa</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">0</td>
         </tr>
         <tr>
          <td class="col-0">20</td>
          <td class="col-4">Nama Wajib Pajak</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">Smk Negeri 1 Rawamerta</td>
         </tr>
         <tr>
          <td class="col-0">21</td>
          <td class="col-4">NPWP</td>
          <td class="col-0">:</td>
          <td colspan="4" class="col">665975843408000</td>
         </tr>
        </table>
       </div>
      </div>
     </div>
     <div class="card">
      <div class="card-header p-0 bg-transparent" id="headingTwo">
       <h2 class="mb-0">
        <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Sejarah SMKN 1 Rawamerta
        </button>
       </h2>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#construction-accordion">
       <div class="card-body">
        <p>Berawal dari sebuah pandangan atau gagasan yang disampaikan oleh beberapa tokoh masyarakat, bahwa di Kecamatan Rawamerta sudah saatnya memiliki SLTA Negeri. Mengingat kecamatan lain, seperti : Kecamatan Tempuran, Pangkalan, Pakisjaya, Tirtajaya, Jayakerta, Kutawaluya, dan Wadas. Masing-masing telah memiliki SLTA Negeri.</p>
        <p>Berdasarkan gagasan tersebut, kemudian ditindaklanjuti dengan diadakannya rapat pada tanggal 5 September 2010. Hasilnya disepakati bahwa di Kecamatan Rawamerta harus berdiri sebuah SLTA yang bernama SMKN 1 Rawamerta. Pada rapat tersebut, selain kesepakatan berdirinya sebuah sekolah juga dibentuknya Tim Pendiri atau Panitia Berdirinya SMKN 1 Rawamerta.</p>
        <p>Tanggal 26 Februari 2011 proposal pendirian SMKN 1 Rawamerta diajukan oleh tim pendiri ke Bupati Karawang Bapak Drs. H. Ade Swara, MH. Beliau memberikan apresiasi dan mendukung keinginan masyarakat Rawamerta untuk didirikannya SMKN 1 Rawamerta, serta mengarahkan agar tim bertemu dengan Kapala Dinas Pendidikan, Pemuda dan Olah Raga Bapak Drs. H. Eka Sanatha, M.Si. untuk dikonsultasikan.</p>
        <p>Tanggal 14 Maret 2011 tim bertemu dengan Kepala Dinas Pendidikan, Pemuda dan Olah Raga. Dan pertemuan berlangsung selama 25 menit. Selain Kepala Dinas, pertemuan itu juga dihadiri oleh Kabid Dikmen Dra. Hj. Nining Ratnaningsih. Dalam pertemuan tersebut, Kepala Dinas menyarankan agar menunda pendirian SMKN 1 Rawamerta. Mengingat sudah ada usulan pendirian SMAN 1 Rawamerta. Berdasarkan saran tersebut, akhirnya tim pun mengadakan rapat pada tanggal 24 Mei 2011. Dan memutuskan rencana berdirinya SMKN 1 Rawamerta ditunda.</p>
        <p>Kemudian pada tanggal 5 September 2011 tim pendiri SMKN 1 Rawamerta menghadap Kepala Dinas Pendidikan, Pemuda dan Olah Raga yang baru Bapak Drs. H. Deden Thosin Waskita, M.Pd. dan Kabid Dikmen Drs. H. Satibi Muhtar, M.Si. Pada pertemuan tersebut mengemuka adanya sebagian masyarakat yang mengusulkan pendirian SMK Rawagede. Yang rencananya pendirian SMK tersebut menggunakan dana hibah dari Negara Belanda. Sesuai dengan yang tercantum dalam bantuan hibah untuk Rakyat Rawagede, yaitu bantuan hibah untuk membangun Rumah Sakit, Pasar, dan Sekolah.</p>
        <p>Lima hari setelah pertemuan dengan Kepala Dinas dan Kabid Dikmen. Tim menghadap Bapak Bupati untuk melaporkan hasil pertemuan tersebut. Kemudian Bapak Bupati menyarankan agar tim bersabar dan tetap berusaha untuk menyiapkan berbagai keperluan dan persyaratan dalam pendirian sekolah.</p>
        <p>Setelah menemui Bapak Bupati dan Kepala Dinas, dengan membawa proposal tim pendiri juga menemui :</p>
        <p>1. Tim Banggar Pemerintah Kabupaten yang diwakili oleh Kabag Umum dan Perlengkapan Bapak Drs. H. Hadis Herdiana.</p>
        <p>2. DPRD kabupaten yang pada waktu itu ditemui oleh Wakil Ketua DPRD Kabupaten Karawang Bapak H. Ahmad Rifa’i, SE. </p>
        <p>3. Ketua Banggar DPRD Bapak Ir. Bambang Marwoto, agar SMKN 1 Rawamerta dapat masuk dalam DIPA 2012. Dan Beliau mendukung SMKN 1 Rawamerta dimasukan lahan dalam DIPA 2012.</p>
        <p>Setelah SMKN 1 Rawamerta masuk dalam DIPA anggaran 2012. Pada tanggal 12 Februari 2012 tim kembali menemui Kepala Dinas Pendidikan, Pemuda dan Olah Raga Bapak Drs. H. Agus Supriatman, M.Pd. dan Kabid Dikmen Drs. H. Satibi Muhtar, M.Si. Dalam pertemuan tersebut, Beliau menyampaikan bahwa SMKN 1 Rawamerta insya Allah bisa berdiri tahun 2012.</p>
        <p>Tanggal 1 Mei 2012 Kepala Dinas Pendidikan, Pemuda dan Olah Raga Bapak Drs. H. Agus Supriatman, M.Pd. mengundang tim pendiri SMKN 1 Rawamerta, Camat Kecamatan Rawamerta, Kepala SMK Nihayatul Amal, Kepala SMK PGRI Rawamerta, Kepala MA Nihayatul Amal, Kepala BPMPD Drs. Sinabang, Kepala Desa Rawagede, Wakil Ketua I DPRD Kab. Karawang, dan Kepala SMK N 1 Karawang. Pada pertemuan itu dibahas tentang perkiraan atau prediksi jumlah siswa lulusan SLTP Tahun 2011/2012 se-Kecamatan Rawamerta sebanyak 1285 orang. Maka wajar jika di Kecamatan Rawamerta berdiri dua sekolah baru. Dan pada pertemuan itu juga dikemukakan oleh Kepala Dinas bahwa SMKN 1 Rawamerta telah resmi berdiri. Dan diinstruksikan kepada seluruh jajaran pemerintahan di Kecamatan Rawamerta agar mensosialisasikan berdirinya SMKN 1 Rawamerta.</p>
        <p>Kemudian hal itu disusul dengan Surat Keputusan Bupati Karawang nomor : 421.3/Kep. 594 – Huk / 2012. Tertanggal 1 Juni 2012 tentang Unit Sekolah Baru (USB) Sekolah Menengah Kejuruan Negeri (SMKN 1 Rawamerta). Dan Tahun Pelajaran 2012/2013 dimulai Penerimaan Peserta Didik Baru. Dengan program keahlian Teknik Komputer dan Jaringan (TKJ) dan Teknik Sepeda Motor (TSM). </p>
        <p>Tokoh masyarakat yang sebelumnya sebagai Tim Pendiri SMKN 1 Rawamerta ditetapkan menjadi Komite Sekolah berdasarkan Surat Keputusan Kepala SMKN 1 Rawamerta nomor 001/422/SMKN.1/2012 tentang kepengurusan Komite SMKN 1 Rawamerta, terhitung mulai 17 Juli 2012.</p>
        </p>
       </div>
      </div>
     </div>
     <div class="card">
      <div class="card-header p-0 bg-transparent" id="headingThree">
       <h2 class="mb-0">
        <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         Visi & Misi SMKN 1 Rawamerta
        </button>
       </h2>
      </div>
      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#construction-accordion">
       <div class="card-body">
        <h3 style="font-size:20px">Visi Sekolah :</h3>
        <p>Terwujudnya Sekolah Menengah Kejuruan yang memiliki daya serap tinggi terhadap Kemajuan Teknologi</p><br>
        <h3 style="font-size:20px;">Misi Sekolah :</h3>
        <p>1. Mengembangkan sistem seleksi penerima siswa baru dan melakukan pembinaan pada calon siswa.</p>
        <p>2. Meningkatkan jumlah & kualitas tenaga kependidikan sesuai dengan tuntutan program pembelajaran yang berkualitas.</p>
        <p>3. Mengupayakan pemenuhan kebutuhan sarana & program pendidikan untuk mendukung KBM & hasil belajar siswa</p>
        <p>4. Menjalin kerjasama (Network) dengan lembaga/institusi terkait, masyarakat & dunia usaha/industri dalam rangka pengembangan program pendidikan yang berakar pada budaya bangsa & mengikuti perkembangan IPTEK.</p>
        <p>5. Mengefektifkan KBM yang mengarah pada program pembelajaran berbasis kompetensi.</p>
        <p>6. Perubahan ke sistem baru yang mengintegrasikan pendidikan dan pelatihan kejuruan secara terpadu.</p>
        <p>7. Perubahan dari manajenen terpusat ke pola manajenen mandiri (prinsip desentrelisasi).</p>
        <p>8. Mengupayakan penggunaan bahasa pengantar berbahasa inggris.</p><br>
        <h3 style="font-size:20px;">Tujuan :</h3>
        <p>Mengupayakan pemenuhan kebutuhan sarana dan prasarana pembelajaran teori dan praktik untuk peningkatan kualitas penyelenggaraan pendidikan di SMK Negeri 1 Rawamerta</p>
        <h3 style="font-size:15px;">A. Tujuan dan Sasaran</h3>
        <p>Mengacu kepada rumusan visi dan misi tersebut di atas, pendirian SMK Negeri 1 Rawamerta pada hakekatnya memiliki tujuan dan sasaran sebagai berikut.</p>
        <p>1. Menghasilkan lulusan SMK Negeri 1 Rawamerta sebagai tenaga kerja tingkat menengah untuk mengisi kebutuhan dunia usaha/industri atau lapangan kerja lainnya yang relevan sesuai dengan permintaan bursa kerja maupun untuk membuka usaha sendiri (wirausaha/wiraswasta).</p>
        <p>2. Menyelenggarakan Pendidikan di SMK Negeri 1 Rawamerta sesuai dengan tuntutan dan standarisasi lapangan kerja, pengetahuan dan sikap professional sesuai dengan perkembangan ilmu dan pengetahuan.</p>
        <p>3. Meningkatkan jumlah dan kualifikasi tenaga kependidikan sesuai dengan Program Pembelajaran yang berkualitas.</p>
        <p>4. Mensosialisasikan kepada masyarakat luas tentang Penyelenggaraan Pendidikan di SMK Negeri 1 Rawamerta termasuk eksistensi dan kinerja Program Keahlian yang diselenggarakan.</p>
        <p>5. Mengupayakan pemenuhan kebutuhan sarana/prasarana pembelajaran teori dan praktik untuk meningkatkan kualitas Penyelenggaraan Pendidikan di SMK Negeri 1 Rawamerta.</p>
        <p>6. Melaksanakan KBM yang mengarah pada Program Pembalajaran berbasis kompetensi.</p>
        <p>Berpijak dari visi, misi serta tujuan dan sasaran penyelenggaraan pendidikan di SMK Negeri 1 Rawamerta, maka SMK Negeri 1 Rawamerta memiliki tujuan dan sasaran yang akan dicapai sebagai berikut:</p>
        <p>1. Tujuan dan sasaran jangka panjang</p>
        <p>a. Pada tahun ketiga jumlah siswa peserta uji kompetensi yang lulus dan dapat terserap oleh dunia usaha/industri yang relevan diperkirakan mencapai 50%.</p>
        <p>b. Pada tahun ketiga jumlah siswa peserta UAN yang memperoleh nilai Matematika  5,6 diperkirakan mencapai 50%.</p>
        <p>c. Pada tahun ketiga jumlah siswa peserta UAN yang memperoleh nilai Bahasa Inggris  7,00 diperkirakan mencapai 50%.</p>
        <p>d. Pada tahun ketiga dapat membangun dan menyelenggarakan badan usaha siswa yang dikembangkan sekolah untuk dikelola oleh para tamatan dalam bentuk pertokoan sekolah, warung telekomunikasi dan internet di lingkungan sekolah.</p>
        <p>e. Pada tahun ketiga dapat melaksanakan uji coba penyelenggaraan KBM satu program diklat dengan menggunakan Bahasa Inggris sebagai pengantar pembelajaran di kelas.</p>
        <p>2. Tujuan dan sasaran jangka pendek </p>
        <p>a. Pada tahun pertama jumlah siswa peserta uji kompetensi yang lulus dapat terserap oleh dunia usaha/industri yang relevan mengalami peningkatan.</p>
        <p>b. Pada tahun pertama jumlah siswa peserta UAN yang memperoleh nilai Matematika  5,6 mencapai 50% atau lebih.</p>
        <p>c. Pada tahun pertama jumlah siswa peserta UAN yang memperoleh nilai Bahasa Inggris  7,00 mencapai 50% atau lebih.</p>
        <p>d. Pada tahun pertama mampu berprestasi dalam even PKS tingkat kabupaten sesuai program keahlian yang diselenggarakan.</p>
        <p>Dengan mengacu dari tujuan dan sasaran tersebut di atas maka banyak tantangan yang harus dihadapi. Peningkatan manajemen pendidikan harus terus diperbaiki kinerjanya secara periodik dan berkesinambungan agar diharapkan SMK Negeri 1 Rawamerta betul-betul dapat menjadi SMK Negeri di Kabupaten Karawang. Karena itu, proposal ini dibuat dan diusulkan untuk menjadi perhatian dan pertimbangan sehingga SMK Negeri 1 Rawamerta dapat didirikan sesuai dengan potensi wilayahnya sehingga apa yang menjadi sasaran dan tujuan fungsional pembelajaran lebih dapat tercapai secara proporsional, profesional dan produktif.</p>
       </div>
      </div>
     </div>
    </div>

   </div><!-- Col end -->

  </div><!-- Content row end -->

 </div><!-- Container end -->
</section><!-- Main container end -->

@endsection